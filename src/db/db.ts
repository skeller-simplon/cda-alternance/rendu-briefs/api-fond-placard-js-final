import dotenv from "dotenv";
dotenv.config();
import mysql from "mysql";

const dbConnection = mysql.createConnection({
  host: "localhost",
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.DB_NAME,
});

dbConnection.connect((err: Error) => {
  if (err) throw err;

  // Création de la table recipe si elle n'existe pas
  checkTableOfCreateIt(
    "Recipe",
    `CREATE TABLE Recipe (
      id INT NOT NULL AUTO_INCREMENT,
      name VARCHAR(45) NOT NULL,
      category VARCHAR(45) NULL,
      picture VARCHAR(255) NOT NULL,
      score INT NULL,
      PRIMARY KEY (id)
    );`
  );

  // Création de la table Ingredient si elle n'existe pas
  checkTableOfCreateIt(
    "Ingredient",
    `CREATE TABLE Ingredient (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(45) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX name_UNIQUE (name ASC))`
  );

  // Création de la table Recipe_Ingredient si elle n'existe pas
  checkTableOfCreateIt("Recipe_Ingredient",
    `CREATE TABLE Recipe_Ingredient (
    Id INT NOT NULL AUTO_INCREMENT,
    RecipeId INT NOT NULL,
    IngredientId INT NOT NULL,
    PRIMARY KEY (Id),
    UNIQUE INDEX Id_UNIQUE (Id ASC),
    INDEX fk_Recipe_Ingredient_Recipe_idx (RecipeId ASC),
    INDEX fk_Recipe_Ingredient_Ingredient_idx (IngredientId ASC),
    CONSTRAINT fk_Recipe_Ingredient_Ingredient
      FOREIGN KEY (IngredientId)
      REFERENCES Ingredient (id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
    CONSTRAINT fk_Recipe_Ingredient_Recipe
      FOREIGN KEY (RecipeId)
      REFERENCES Recipe (id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION)
    `);
});

function checkTableOfCreateIt(tableName: string, query: string) {
  dbConnection.query(
    "SHOW TABLES LIKE '" + tableName + "'",
    (error, results) => {
      if (error) return console.log(error);
      if (!results.length) {
        console.log(`Création de la table ${tableName}`);
        dbConnection.query(query);
      }
    }
  );
}

export default dbConnection;
