export const toInt = (str: string) => {
    return parseInt(str, 10) || null;
}