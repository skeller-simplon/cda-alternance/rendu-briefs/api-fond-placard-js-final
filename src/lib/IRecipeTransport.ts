export default interface IRecipeTransport {
    name: string;
    category: string;
    picture: string;
    score: number;
    ingredients: number[]
}

