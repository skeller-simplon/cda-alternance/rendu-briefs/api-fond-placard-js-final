export default class Recipe {
    public id: number;
    public name: string;
    public category: string;
    public picture: string;
    public score: number;
}

