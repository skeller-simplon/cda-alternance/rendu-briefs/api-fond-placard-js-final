export default class IError extends Error {

    constructor(message: string, code: number = 400, type: Error = IError.prototype) {
        super(message)
        this.code = code;
        this.type = type;
    }
    code: number;
    type: Error;
}