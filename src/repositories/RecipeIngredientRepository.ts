import Ingredient from "../lib/Ingredient";
import BaseRepository from "./BaseRepository";
import RecipeIngredient from "../lib/RecipeIngredient"

export default class RecipeIngredientRepository extends BaseRepository<Ingredient> {
    constructor() {
        super();
        this.tableName = "Recipe_Ingredient";
    }

    createRecipeIngredient(recipeId: number, ingredientId: number): Promise<RecipeIngredient> {
        return new Promise((res, rej) => {
            this.connection.query(`INSERT INTO ${this.tableName} (RecipeId, IngredientId) VALUES (?, ?)`,
                [recipeId, ingredientId], (err, [servRes]) => {
                    res(servRes)
                })
        })
    }
}