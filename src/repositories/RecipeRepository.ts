import Recipe from "../lib/Recipe";
import BaseRepository from "./BaseRepository";

export default class RecipeRepository extends BaseRepository<Recipe> {
    constructor() {
        super();
        this.tableName = "Recipe";
    }
    // CRUD
    getOneRecipeById = async (id: number) => { return await this.getOneById(id) }
    getAllRecipes = async () => { return await this.getAll() }

    // Récupères toutes les recettes par une list d'id d'ingrédients
    getRecipesByIngredientsIds(ingredients: string[]) {
        return new Promise((res, rej) => {
            this.connection.query(`SELECT * FROM Recipe WHERE id IN(SELECT RecipeId FROM Recipe_Ingredient WHERE IngredientId in (${ingredients.join(", ")}))`,
                (err, servRes) => { res(servRes) })
        })
    }

    createRecipe(recipe: Recipe): Promise<Recipe> {
        return new Promise((res, rej) => {
            this.connection.query(`INSERT INTO ${this.tableName} (name, category, picture, score) VALUES (?, ?, ?, ?);`,
                [recipe.name, recipe.category, recipe.picture, recipe.score],
                (err, servRes: Recipe) => { res(servRes) })
        })
    }
}