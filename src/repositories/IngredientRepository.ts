import Ingredient from "../lib/Ingredient";
import Recipe from "../lib/Recipe";
import BaseRepository from "./BaseRepository";

export default class IngredientRepository extends BaseRepository<Ingredient> {
    constructor() {
        super();
        this.tableName = "Ingredient";
    }
    // CRUD
    getOneIngredientById = async (id: number) => { return await this.getOneById(id) }
    getIngredientsRecipes = async () => { return await this.getAll() }

}