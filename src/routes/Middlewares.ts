import IError from "../lib/IError";

export default class Middlewares {

    static checkBody = (arr: string[]) => Middlewares.checkParamsWithSource(arr, "body")
    static checkParams = (arr: string[]) => Middlewares.checkParamsWithSource(arr, "params")
    static checkQuery = (arr: string[]) => Middlewares.checkParamsWithSource(arr, "query")

    private static checkParamsWithSource(arr: string[], source: string) {
        return function (req: any, res: any, next: any) {
            const missing_params = [];
            for (let i = 0; i < arr.length; i++) {
                if (!eval(`req.${source}.${arr[i]}`)) {
                    missing_params.push(arr[i]);
                }
            }
            if (missing_params.length == 0) {
                next();
            } else {
                throw new IError("Parametre(s) manquant: " + missing_params.join(","), 402)
            }
        }
    }
}