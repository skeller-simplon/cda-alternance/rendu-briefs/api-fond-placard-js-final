import express from 'express'
const router = express()

import RecipeController from "./RecipeController";

router.use("/recipes", RecipeController);

export default router;
