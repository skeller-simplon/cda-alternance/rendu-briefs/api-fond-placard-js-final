import express from "express";
import { toInt } from "../utils/utils";
import RecipeService from "../business/RecipeService";
import IError from "../lib/IError"
import Recipe from "../lib/Recipe";
import Middlewares from "./Middlewares"

const RecipeController = express.Router();
const _recipeService = new RecipeService();

RecipeController.route("/").get((req, res) => {
    _recipeService.getAllRecipes().then(recipes => {
        res.json(recipes)
    });
}).post(Middlewares.checkBody(["name", "category", "picture", "ingredients"]), (req, res) => {
    let bodyRecipe = new Recipe();
    let ingredients: number[] = [];

    bodyRecipe.name = req.body.name
    bodyRecipe.category = req.body.category
    bodyRecipe.picture = req.body.picture

    if (!isNaN(req.body.score))
        bodyRecipe.score = parseInt(req.body.score)
    else
        bodyRecipe.score = 0

    for (const ingredient of req.body.ingredients) {
        if (isNaN(ingredient) || typeof ingredient !== "number")
            throw new IError("Id d'ingrédient incorrecte")
        else
            ingredients.push(ingredient);
    }
    try {
        _recipeService.createRecipe(bodyRecipe, ingredients).then((recipe) => {
            res.send(recipe);
        }).catch(err => { throw err })
    } catch (error) {
        if (error instanceof IError)
            res.status(error.code).send(error.message)
        else
            res.status(400).send(error.message)
    }
})

RecipeController.route("/:id").get((req, res) => {
    _recipeService.getOneRecipeById(toInt(req.params.id)).then(recipe => {
        res.json(recipe)
    });
})
RecipeController.route("/byIngredientsIds").post((req, res) => {
    try {
        _recipeService.getRecipeByIngredientList([...req.query.ingredient as string[]]).then(recipes => {
            res.status(200).send(recipes)
        })
    } catch (error) {
        if (error instanceof IError)
            res.status(error.code).send(error.message)
        else
            res.status(400).send(error.message)
    }

})

export default RecipeController