import BaseService from "./BaseService";
import RecipeRepository from "../repositories/RecipeRepository"
import IngredientService from "./IngredientService"
import RecipeIngredientService from "./RecipeIngredientService"
import IError from "../lib/IError";
import Recipe from "../lib/Recipe";

export default class RecipeService extends BaseService {
    private repository: RecipeRepository = null;
    private _ingredientService: IngredientService = null
    private _recipeIngredientService: RecipeIngredientService = null

    constructor() {
        super();
        this.repository = new RecipeRepository();
        this._ingredientService = new IngredientService();
        this._recipeIngredientService = new RecipeIngredientService();
    }
    getOneRecipeById(param: number) {
        return this.repository.getOneRecipeById(param)
    }

    getAllRecipes() {
        return this.repository.getAllRecipes();
    }

    getRecipeByIngredientList(ingredients: string[]) {
        // Si pas de query, la requête n'est pas correctement formée
        if (!ingredients) {
            throw new IError("Aucun ingrédient fourni.")
        }

        // Vérifie que les ingrédients sont des numéros (ids)
        for (const ingredient of ingredients) {
            if (!parseInt(ingredient))
                throw new IError("Mauvais format d'ingredient")
        }

        return this.repository.getRecipesByIngredientsIds(ingredients);
    }

    async createRecipe(recipe: Recipe, ingredients: number[]): Promise<Recipe> {
        try {
            // Vérification ingrédients
            for (const ingredientId of ingredients) {
                let ingredient = await this._ingredientService.getOneRecipeById(ingredientId)
                if (ingredient === undefined)
                    throw new IError(`L'ingrédient d'id ${ingredientId} n'existe pas`)
            }
            this.repository.createRecipe(recipe).then(async recipe => {
                for (const ingredientId of ingredients) {
                    await this._recipeIngredientService.createRecipeIngredient(recipe.id, ingredientId)
                    return recipe
                }
            })
        } catch (error) {
            throw error
        }
        return null
    }
}