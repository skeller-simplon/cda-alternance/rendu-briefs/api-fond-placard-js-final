import BaseService from "./BaseService";
import IngredientRepository from "../repositories/IngredientRepository";

export default class IngredientService extends BaseService {
    private repository: IngredientRepository = null;
    constructor() {
        super();
        this.repository = new IngredientRepository();
    }
    getOneRecipeById(id: number) {
        return this.repository.getOneIngredientById(id)
    }


}