import BaseService from "./BaseService";
import RecipeIngredientRepository from "../repositories/RecipeIngredientRepository";
import RecipeIngredient from "../lib/RecipeIngredient";

export default class IngredientService extends BaseService {
    private repository: RecipeIngredientRepository = null;
    constructor() {
        super();
        this.repository = new RecipeIngredientRepository();
    }

    createRecipeIngredient(recipeId: number, ingredientId: number): Promise<RecipeIngredient> {
        return this.repository.createRecipeIngredient(recipeId, ingredientId)
    }

}